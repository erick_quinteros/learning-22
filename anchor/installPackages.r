##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install needed packages for anchor learning
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
# modified on: 2015-10-27 - made multi-day and cleanup up
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
#
# install Packages needed for Anchor code
# build function library used by the Anchor driver code
#
##########################################################
args <- commandArgs(TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args)==0){
    print("No arguments supplied.")
    quit(save = "no", status = 1, runLast = FALSE)
}else{
    print("Arguments supplied.")
    for(i in 1:length(args)){
      eval(parse(text=args[[i]]))
      print(args[[i]]);
    }
}

# install packages needed by the Anchor functions

needed <- c("futile.logger","RMySQL","data.table","reshape2","markovchain","Rsolnp","lattice","bizdays","geosphere","testthat","Rcpp","properties","openxlsx","doRNG")
options(downLoad.file.method="curl")
inst <- installed.packages()
for(i in needed)
  {
      if(!is.element(i,inst))
          {
              install.packages(i,repos="https://cran.rstudio.com/")
          }
  }

# R CMD build learningPackage
shellCode <- sprintf("tar -cvf Learning.tar.gz %s/learningPackage",homedir)
system(shellCode)

if(is.element("Learning",inst))remove.packages("Learning")
# R CMD INSTALL Learning_1.0.tar.gz 
install.packages("Learning.tar.gz", repos = NULL, type = "source")


# R CMD build dataAccessLayer
shellCode <- sprintf("tar -cvf Learning_DataAccessLayer.tar.gz %s/dataAccessLayer",homedir)
system(shellCode)

if(is.element("Learning.DataAccessLayer",inst))remove.packages("Learning.DataAccessLayer")
# R CMD INSTALL Learning.DataAccessLayer_1.0.tar.gz 
install.packages("Learning_DataAccessLayer.tar.gz", repos = NULL, type = "source")
