from awsglue.transforms import *
from utils import Utils
import logging
from pyspark.sql.functions import *
from pyspark.sql.functions import *
import pyspark

logger = None
#global str

class DseRecordClassTrans:
    def __init__(self, glue_context, customer, s3_destination, deltaTable):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session
        self.DeltaTable = deltaTable

        global logger
        logger = logging.getLogger("ADL_DEV_LOGGER")
        pass


    def run(self, batch, upsertDate):
        try:
            if batch:
                df_rc = self.utils.dataLoadDelta(self.s3_destination + 'data/silver/recordclasses')
                #df_rc.cache()
                df_rc.createOrReplaceTempView("rc")
                logger.info("rc cached")
            else:
                logger.info("rc loaded in previous step")

            df_rst = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/rep_team_rep')
            df_rst.createOrReplaceTempView("rst")
            logger.info("df_rst cached")

            df_dse = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/dse_score')
            if not batch:
                df_dse = df_dse.filter(df_dse.startDateLocal > upsertDate)
            df_dse.createOrReplaceTempView("dse")
            logger.info("df_dse cached")


            #DSE data processing
            spark_str="""
              select distinct t1.suggestedyear, t1.suggestedmonth, t1.suggestedday, t1.repId, t1.accountId, t1.suggestedDate, t1.runId, t1.runRepDateId, t1.startDateTime, t1.startDateLocal, t1.seConfigId, t1.runGroupId, t1.numSuggestibleReps, t1.numSuggestibleAccounts, t1.numEvaluatedAccounts, t1.numErrorsAndWarnings, t1.isSuggestionCritical, t1.suggestionPriorityScore, t1.suggestionReferenceId, t1.runRepDateSuggestionId, t1.suggestionRank, t1.isEligibleForLoadingIntoCRM, t1.isEligibleForLoadingIntoMAS, t1.actionValue, t1.isSupersededByTrigger, t1.primaryRepActionTypeId, t1.suggestedRepActionTypeUID, t1.detailRepActionTypeId, t1.detailRepActionTypeUID, t1.messageTopicId
            from dse t1 
            inner join (
            select ss.suggestionReferenceId,max(ss.runId) as runId,max(ss.startDateTime) as startDateTime,rd.suggestedDate,ss.repId,ss.accountId
            from rc rd
            inner join dse ss 
            on rd.suggestionReferenceId=ss.suggestionReferenceId and rd.suggestedDate=ss.suggestedDate
            and rd.suggestionAccountId=ss.accountId
            group by ss.suggestionReferenceId,rd.suggestedDate,repId,accountId
            ) t2 
            on t1.suggestionReferenceId=t2.suggestionReferenceId and t1.suggestedDate=t2.suggestedDate
            and t1.runId=t2.runId and t1.startDateTime=t2.startDateTime and t1.repId=t2.repId and t1.accountId=t2.accountId
            """
            df=self.spark.sql(spark_str)
            df.createOrReplaceTempView("dse_2")


            spark_str="""
            select rd.*,t.runId as dseRunId,t.isSuggestionCritical as dseIsAccountCritical,
            '' as dseNumWorkDaysOverdue, '' as dseCurrentCompletionsPeriodTargetsProgressStatusId, '' as dseEvaluatedCompletionsPeriodTargetsProgressStatusId,
            '' as dseAllTargetsProgressText,'' as dseTargetDrivenEvaluationInfo,
            t.startDateTime as dseRunStartDateTime,t.startDateLocal as dseRunStartDateLocal,t.numSuggestibleReps as dseNumSuggestibleReps,
            t.numSuggestibleAccounts as dseNumSuggestibleAccounts,t.numEvaluatedAccounts as dseNumEvaluatedAccounts,
            t.runRepDateSuggestionId as dseRunRepDateSuggestionId,t.suggestionPriorityScore as dseSuggestionPriorityScore,t.isSuggestionCritical as dseIsSuggestionCritical,t.isSupersededByTrigger as dseIsSupersededByTrigger
            from rc rd 
            left join 
            (
            select * from dse_2
            ) t 
            on rd.suggestionReferenceId=t.suggestionReferenceId and rd.suggestedDate=t.suggestedDate
            and rd.suggestionAccountId=t.accountId
            """
            df_rcv2=self.spark.sql(spark_str)
            logger.info(df_rcv2.count())
            df_rcv2.createOrReplaceTempView("rc2")
            #add dates information
            df_dates= self.spark.read.csv("s3://" + self.s3_destination.split('/')[2] + "/adl/common/data/dates.csv", sep='|', header='true')
            df_dates = self.utils.normalizeColumnNames(df_dates)
            df_dates.createOrReplaceTempView("dates")
            spark_str="""
            
            select *,COALESCE(suggestionYearMonth,interactionYearMonth) as allEffectiveyearmonth,CONCAT(`allRepId`, '_', `allAccountId`) as allRepAccount,year(allEffectivedate) as allYear,
            month(allEffectivedate) as allMonth,DAYOFMONTH(allEffectivedate) as allDay from (
            select 
            recordclassId, recordclass, interactionId, externalId, interactionTimeZoneId, interactionyear, interactionmonth, interactionday, interactionStartDateTime, interactionStartDateLocal, 
            d.weekDayName as interactionWeekDayName,d.isWeekend interactionIsWeekend,d.dOWInMonth as interactionDOWInMonth,
            d.dayOfYear as interactionDayOfYear,d.weekOfMonth as interactionWeekOfMonth,d.weekOfYear as interactionWeekOfYear,
            concat(left(SUBSTRING_INDEX(d.`date`,'-',2),4),right(SUBSTRING_INDEX(d.`date`,'-',2),2)) interactionYearMonth,
            d.quarter as interactionQuarter, 
            interactionTypeId, interactionTypeName, repActionTypeId, repActionTypeName, interactionDuration, interactionWasCreatedFromSuggestion, 
            interactionIsCompleted, interactionIsDeleted, interactionCreatedAt, interactionUpdatedAt, interactionRepId, interactionAccountId, CONCAT(`interactionRepId`, '_', `interactionAccountId`) as interactionRepAccount,
            interactionFacilityId, interactionAccountIsdeleted, rawCsId, emailEmailSentDate, emailCaptureDatetime, 
            emailAccountuid, emailAccountEmail, emailEmailConfigValues
            ,emailEmailSubject
            ,emailEmailContent
            --, Lilly_Content1__c
            ,emailOpened, emailOpenCount, 
            emailClickCount, emailProductDisplay, emailProduct, emailSenderEmail, 
            emailStatus, emailMessageId,emailMessageChannelId,emailMessageTopicId,emailMessageTopicName,emailMessagename,emailMessagedescription,emailCall2, callCallDatetime, callCallDate, callAccountuid, callAddressLine1, callState, callCity, callZip4, callZip, callAddressLine2, 
            callNextCallNotes,
             callCallType, callAttendeeType, 
            callDetailedProducts, 
            callSubmittedByMobile, productInteractionTypeId, productInteractionTypeName, messageId, messageTopicId, 
            -- messageTopicName, 
            messageReaction, physicalMessageDesc, physicalMessageUID, quantity,  
            suggestionReferenceId, suggestionUID, suggestedDate, ds.weekDayName as suggestionWeekDayName,ds.isWeekend as suggestionIsWeekend,ds.dOWInMonth as suggestionDOWInMonth,
            ds.dayOfYear as suggestionDayOfYear,ds.weekOfMonth as suggestionWeekOfMonth,ds.weekOfYear as suggestionWeekOfYear
            ,concat(left(SUBSTRING_INDEX(ds.`date`,'-',2),4),right(SUBSTRING_INDEX(ds.`date`,'-',2),2)) suggestionYearMonth,ds.quarter as suggestionQuarter ,suggestionChannel, suggestionActionTaken, suggestionDismissReason, 
            suggestionInteractionraw, suggestionInteractionId, suggestionRepId,suggestionAccountId,CONCAT(`suggestionRepId`, '_', `suggestionAccountId`) as suggestionRepAccount, suggestionDetailRepActionTypeId, suggestionDetailRepActionName, 
            suggestionMessageId, suggestionMessageName, suggestionRepTeamId, suggestionProductId, suggestedyear, 
            suggestedmonth, suggestedday, dseRunId, dseIsAccountCritical, dseNumWorkDaysOverdue, dseCurrentCompletionsPeriodTargetsProgressStatusId, 
            dseEvaluatedCompletionsPeriodTargetsProgressStatusId, dseAllTargetsProgressText, dseTargetDrivenEvaluationInfo, dseRunStartDateTime, 
            dseRunStartDateLocal, dseNumSuggestibleReps, dseNumSuggestibleAccounts, dseNumEvaluatedAccounts, dseRunRepDateSuggestionId, 
            dseSuggestionPriorityScore, dseIsSuggestionCritical, dseIsSupersededByTrigger,
            COALESCE(rdd.interactionRepId,suggestionRepId) as allRepId,
            COALESCE(rdd.interactionAccountId,suggestionAccountId) as allAccountId,
            cast(COALESCE(suggestedDate,rdd.interactionStartDateLocal) as date) allEffectivedate
             from rc2 rdd
            left join dates d
            on rdd.interactionStartDateLocal=d.`date`
            left join dates ds
            on rdd.suggestedDate=ds.`date`
            ) t 
            """

            df_rcv3=self.spark.sql(spark_str)
            df_rcv3.createOrReplaceTempView("rc3")
            #df_rcv3.repartition(40).write.mode('overwrite').partitionBy("All_year").format("delta").save(self.s3_destination+"data/bronze/dse_score_final/")

            #finally check repteam alignment and get repids
            df_rcv4 = df_rcv3.join(df_rst,((df_rcv3.allRepId == df_rst.repId) & (df_rcv3.allEffectivedate.between(df_rst.startDate,df_rst.endDate))),'left_outer').select(df_rcv3["*"],df_rst["repTeamId"],df_rst["cluster"]).dropDuplicates()
            df_rcv4.createOrReplaceTempView("rcv4")
            #df_rcv4.repartition("All_year", "All_month", "All_day").write.mode('overwrite').partitionBy("All_year", "All_month", "All_day").format("delta").save(self.s3_destination + "data/silver/recordclasses_dse/")
            if batch:
                df_rcv3.repartition(80).write.mode('overwrite').format("delta").save(
                    self.s3_destination + "data/bronze/dse_score_final/")
                df_rcv4.repartition(40).write.mode('overwrite').partitionBy("allYear","recordclassId").format(
                "delta").save(self.s3_destination + "data/silver/recordclasses_dse/")
            else:
                # logger.info(df_rcv3.explain())
                # self.utils.upsertRecordClass(df_rcv3, self.s3_destination+"data/bronze/dse_score_final/", self.DeltaTable)
                logger.info("dse_score_final is versioned")
                # self.utils.upsertRecordClass(df_rcv4, self.s3_destination+ "data/silver/recordclasses_dse/", self.DeltaTable)
                logger.info("recordclasses_dse is versioned")
                df_rcv4.withColumn('allEffectivedate', to_date(df_rcv4.allEffectivedate, 'yyyy-MM-dd'))
                df_rcv4.persist(pyspark.StorageLevel.MEMORY_ONLY)
                logger.info("dse recordclass size " + str(df_rcv4.count()))
                broadcast(df_rcv4)
                df_rcv4.createOrReplaceTempView("rc")

        except Exception as e:
            logger.error("Error in dseRecordClassTrans: {}".format(e))
            raise
