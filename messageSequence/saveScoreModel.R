
##########################################################
#
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: save messageSequence results
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(Hmisc)
library(data.table)
library(futile.logger)

saveScoreModelDB <- function(isNightly, con, con_l, scores, whiteList, RUN_UID, BUILD_UID, CONFIG_UID, msgAlgoId, messageRescoringTimesNew, resetScoreTable=FALSE)
{
    flog.info("entered saveScoreModel function")    

    #subset scores using whitelist
    scores <- scores[accountId %in% whiteList]
    
    flog.info("Number of rows in scores returned from scoreModel = %s", nrow(scores))

    if (isNightly) # nightly/publishing job
    {
      scores$messageAlgorithmId <- msgAlgoId
      scores$modelId <- CONFIG_UID
      scores$predict <- 1
      scores[,probability:=prob*AUC]          # measure of confidence of prediction
      scores$messageId <- NULL                # this messageId is actually messageName we don't need

      setnames(scores, "msgId", "messageId")  # rename msgId to messageId

      print(describe(scores))
      # done with debug stuff

      FIELDS <- list(accountId="INT", messageAlgorithmId="INT", messageId="INT", modelId="varchar(80)", learningRunUID="varchar(80)", probability="double", predict="tinyint", createdAt="datetime", updatedAt="datetime")
   
      scores <- scores[, list(accountId, messageAlgorithmId, messageId, modelId, probability, predict)]

      scores[, c("learningRunUID") := list(RUN_UID)]      # add learningRunUID

      nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      scores$createdAt <- nowTime
      scores$updatedAt <- nowTime
      
      if (resetScoreTable) { # truncate AccountMessageSequence when save the first nighlty scoring results
        startTimer <- Sys.time()
        
        tryCatch(dbGetQuery(con, "TRUNCATE AccountMessageSequence;"),
                 error = function(e) {
                   flog.error('Error in truncate AccountMessageSequence in DSE: %s', e, name='error')
                   dbDisconnect(con)
                   quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
                 })   
        
        flog.info("Delete old scores of msgAlgoId Time = %s", Sys.time()-startTimer)
        flog.info("Number of rows in scores = %s", nrow(scores))
      }
      
      startTimer <- Sys.time()
      # write back to table
      tryCatch(dbWriteTable(con, name="AccountMessageSequence", value=as.data.frame(scores), overwrite=FALSE, append=TRUE, row.names=FALSE, field.types=FIELDS),
               error = function(e) {
                 flog.error('Error in writing back to table AccountMessageSequence in DSE!: %s', e, name='error')
                 dbDisconnect(con)
                 quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
               })   
      flog.info("Write scores to DSE AccountMessageSequence Time = %s", Sys.time()-startTimer)
      
      ## update MessageRescoringTimes table
      if (nrow(messageRescoringTimesNew)>0) {
        
        startTimer <- Sys.time()
        # delete records for builds in rsBuids
        deleteRsBuildsSQL <- sprintf("DELETE FROM MessageRescoringTimes WHERE learningBuildUID='%s'", BUILD_UID)
        dbClearResult(dbSendQuery(con_l, deleteRsBuildsSQL))
        flog.info("Delete old records of MessageRescoringTimes Time = %s", Sys.time()-startTimer)
        
        startTimer <- Sys.time()
        FIELDS <- list(accountId="int", physicalMessageUID="char(40)", messageId="int", learningBuildUID="char(80)", rescoringTimes="int", createdAt="datetime", updatedAt="datetime")
        tryCatch(dbWriteTable(con_l, name="MessageRescoringTimes", value=as.data.frame(messageRescoringTimesNew), overwrite=FALSE, append=TRUE, row.names=FALSE, field.types=FIELDS),
                 error = function(e) {
                   flog.error('Error in writing back to table MessageRescoringTimes in Learning DB!: %s', e, name='error')
                   dbDisconnect(con_l)
                   quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
                 }
        )
        flog.info("Write rescoring count to Learning MessageRescoringTimes Time = %s", Sys.time()-startTimer)
        
      } else {
        flog.info("no rescoring executed, so no change to messageRescoringTimes table")
      }

    } else {  # manual run job
      scores$messageId <- NULL               # this messageId is actually messageName we don't need
      scores[,probability:=prob*AUC]         # measure of confidence of prediction

      setnames(scores, "msgId", "messageId") # rename msgId to messageId

      print(describe(scores))
      # done with debug stuff

      accountIdMap <- data.table(dbGetQuery(con,"SELECT accountId, externalId FROM Account WHERE isDeleted=0;"))
      messageIdMap <- data.table(dbGetQuery(con,"SELECT messageId, externalId FROM Message;"))
    
      FIELDS <- list(learningRunUID="varchar(80)",learningBuildUID="varchar(80)",accountUID="varchar(80)",messageUID="varchar(80)",probability="double", createdAt="datetime", updatedAt="datetime")
    
      # add new columns to scores and assign default values
      scores[,c("learningBuildUID","learningRunUID"):=list(BUILD_UID,RUN_UID)]

      # get accountUID from accountId
      scores <- merge(scores, accountIdMap, by="accountId")
      setnames(scores, "externalId", "accountUID")
      scores$accountId  <- NULL

      # get messageUID from messageId
      scores <- merge(scores, messageIdMap, by="messageId")
      setnames(scores, "externalId", "messageUID")
      scores$messageId  <- NULL

      # pick up needed columns for table in learning DB
      scores <- scores[,list(learningBuildUID,learningRunUID,accountUID,messageUID,probability)]

      nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      scores$createdAt <- nowTime
      scores$updatedAt <- nowTime
      
      flog.info("Append the scores in the AMS table with rows = %s",dim(scores)[1])

      tryCatch(dbWriteTable(con_l, name="AccountMessageSequence", value=as.data.frame(scores), overwrite=FALSE, append=TRUE, row.names=FALSE, field.types=FIELDS),
               error = function(e) {
                 flog.error('Error in writing back to table AccountMessageSequence in Learning DB!: %s', e, name='error')
                 dbDisconnect(con_l)
                 quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
               }
      )
      flog.info("Finish writing scores to Learning AccountMessageSequence") 
    }
}

saveScoreModelExcel <- function(fullScores, pName, WORKBOOK, spreadsheetName) {
  
  flog.info("Save info to workbook")
  t <- unique(fullScores,by=c("messageId","aveRate","breakProb"))
  t <- t[,c("messageId","aveRate","breakProb"),with=F]
  
  addWorksheet(WORKBOOK,pName)
  writeData(WORKBOOK,pName,t,startRow=1)
  statusCode <- tryCatch(saveWorkbook(WORKBOOK, file = spreadsheetName, overwrite = T), 
                         error = function(e) {
                           flog.info("Original error message: %s", e)
                           flog.error('Error in saveWorkbook',name='error')
                           return (NA)
                         })
  
  if (is.na(statusCode)) {
    stop("failed to save output to excel file")
  }
  
  flog.info("finish save scores to excel")
}

saveScoreModelPlot <- function(fullScores, RUN_UID, plotDir) {
  plotLoc <- sprintf("%s/PredictionSummary_%s.pdf",plotDir,RUN_UID)                                    # plot the distribution for a sanity check if needed
  pdf(plotLoc)
  print(histogram(~prob,fullScores,xlab="Likelihood",main="Distribution of likelihood"))
  print(histogram(~prob|predict,fullScores,xlab="Likelihood",main="Distribution of likelihood"))
  print(histogram(~prob|factor(messageId),fullScores,xlab="Likelihood",main="Distribution of likelihood\nby messageId"))
  if(dim(fullScores[predict==0])[1]>10)print(histogram(~prob|factor(messageId),fullScores[predict==0],xlab="Likelihood",main="Distribution of likelihood\nby messageId\n Predict=0"))
  if(dim(fullScores[predict==1])[1]>10)print(histogram(~prob|factor(messageId),fullScores[predict==1],xlab="Likelihood",main="Distribution of likelihood\nby messageId\n Predict=1"))
  print(bwplot(factor(messageId)~prob,fullScores,xlab="Likelihood",main="Distribution of likelihood\nby messageId"))
  if(dim(fullScores[predict==0])[1]>10)print(bwplot(factor(messageId)~prob,fullScores[predict==0],xlab="Likelihood",main="Distribution of likelihood\nby messageId\n Predict=0"))
  if(dim(fullScores[predict==1])[1]>10)print(bwplot(factor(messageId)~prob,fullScores[predict==1],xlab="Likelihood",main="Distribution of likelihood\nby messageId\n Predict=1"))
  m <- unique(fullScores$messageId)
  for(i in m)print(histogram(~prob|predict,fullScores[messageId==i],xlab="Likelihood",main=sprintf("Distribution of likelihood\nby messageId %s",i)))
  dev.off()
  flog.info("finish save pdf to plotDir")
}
