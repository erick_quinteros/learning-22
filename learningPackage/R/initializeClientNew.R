##########################################################
#
#
# LearningPackage Func: initializeClientNew()
#
# description: adopt from original initializeClient()
#              register error handler, logger, print redirect
#              & initialize folder for plots and excel file
#              if needed
#
# created by : shirley.xu@aktana.com
#
# created on : 2018-09-25
#
# Copyright AKTANA (c) 2018.
#
#
##########################################################

initializeClientNew <- function(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=NULL, createExcel=FALSE, createPlotDir=FALSE, createModelsaveDir=FALSE, debugMode=FALSE) {
  # register error handler
  options(error=errorFunc)
  
  runDir <- sprintf("%s/builds/%s",homedir,BUILD_UID)
  
  # register log
  if (stringr::str_detect(DRIVERMODULE, "Score") | stringr::str_detect(DRIVERMODULE, "anchor") | stringr::str_detect(DRIVERMODULE, "engagement")){pref = "Score"} else {pref = "Build"}
  logName <- sprintf("%s/log_%s.txt",runDir,paste(pref,RUN_UID,sep='_'))
  flog.appender(appender.file(logName))
  if (debugMode) {
    flog.threshold(DEBUG)
    flog.info("Run directory = %s (DEBUG mode)",runDir)
  } else {
    flog.threshold(INFO)
    flog.info("Run directory = %s",runDir)
  }
  # forward printed output
  printName <- sprintf("%s/print_%s.txt",runDir,paste(pref,RUN_UID,sep='_'))
  sink(printName)
  
  # create excel workbook for output
  if (createExcel) {
    WORKBOOK <- createWorkbook()
    RunTimeStamp <- sprintf("Run initialized at: %s",Sys.time())
    addWorksheet(WORKBOOK,"RunTimeStamp")
    writeData(WORKBOOK,"RunTimeStamp",RunTimeStamp)
    spreadsheetName <- sprintf("%s/%s_%s_%s.xlsx", runDir, DRIVERMODULE, BUILD_UID, RUN_UID)
  } else {
    WORKBOOK <- NULL
    spreadsheetName <- NULL
  }
  
  # create plot folder
  plotDir <- sprintf("%s/plot_output_%s",runDir,paste(pref,RUN_UID,sep='_'))
  if (createPlotDir) { dir.create(plotDir,showWarnings=T,recursive=T) }
  
  # create modelsave folder
  modelsaveDir <- sprintf("%s/models_%s",runDir,BUILD_UID)
  if (createModelsaveDir) { dir.create(plotDir,showWarnings=T,recursive=T) }
  
  return (list(runDir=runDir,logName=logName,printName=printName,WORKBOOK=WORKBOOK,spreadsheetName=spreadsheetName,plotDir=plotDir,modelsaveDir=modelsaveDir))
}
