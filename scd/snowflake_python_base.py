import logging
import os
import sys

import json
import snowflake.connector
import mysql.connector
from mysql.connector import Error

class snowflake_python_base:

    """
    PURPOSE:
        This is the Base/Parent class for programs that use the Snowflake
        Connector for Python.
        This class is intended primarily for:
            * Sample programs, e.g. in the documentation.
            * Tests.
    """


    def __init__(self, p_log_file_name = None):

        """
        PURPOSE:
            This does any required initialization steps, which in this class is
            basically just turning on logging.
        """

        file_name = p_log_file_name
        if file_name is None:
            file_name = '/tmp/snowflake_python_base.log'

        # -- (> ---------- SECTION=begin_logging -----------------------------
        logging.basicConfig(
            filename=file_name,
            level=logging.INFO)
        # -- <) ---------- END_SECTION ---------------------------------------


    # -- (> ---------------------------- SECTION=main ------------------------
    def main(self, argv):

        """
        PURPOSE:
            Most tests follow the same basic pattern in this main() method:
               * Create a connection.
               * Set up, e.g. use (or create and use) the warehouse, database,
                 and schema.
               * Run the queries (or do the other tasks, e.g. load data).
               * Clean up. In this test/demo, we drop the warehouse, database,
                 and schema. In a customer scenario, you'd typically clean up
                 temporary tables, etc., but wouldn't drop your database.
               * Close the connection.
        """

        # Get the other login info etc. from the command line.
        parameters = self.get_commandline_parameters(argv)

        # Read the connection parameters (e.g. user ID) from the command line
        # and environment variables, then connect to Snowflake.
        connection = self.create_connection(parameters)

        # Set up anything we need (e.g. a separate schema for the test/demo).
        self.set_up(connection, parameters)

        # Do the "real work", for example, create a table, insert rows, SELECT
        # from the table, etc.
        self.process_db(connection, parameters)

        # Clean up. In this case, we drop the temporary warehouse, database, and
        # schema.
        self.clean_up(connection, parameters)

        #print("\nClosing connection...")
        # -- (> ------------------- SECTION=close_connection -----------------
        connection.close()
        # -- <) ---------------------------- END_SECTION ---------------------

    # -- <) ---------------------------- END_SECTION=main --------------------


    def get_metadata_config_filename(self, region):
        filePath = os.path.dirname(os.path.abspath(__file__))
        defaultMetadataFile = 'customer-metadata.json'
        if region != "":
            metadataFile = filePath + "/" + region + '.' + defaultMetadataFile
            if os.path.isfile(metadataFile):
                return metadataFile
        return filePath + "/" + defaultMetadataFile

    def retrieve_metadata(self, connection_parameters):

        """
        PURPOSE:
            Read Aktana metadata for Customer Snowflake info and add it to parameters dictionary.
        INPUTS:
            The dictionary of command line params and place to add new params
        RETURNS:
            Returns the dictionary.
        DESIRABLE ENHANCEMENTS:
            Improve error detection and handling.
        """

        customer = connection_parameters["customer"]
        environment = connection_parameters["env"]

        try:
            region = connection_parameters["region"]
        except:
            region = ""
            
        # get params to connect to metadata db in mysql
        metadataFilePath = self.get_metadata_config_filename(region)
        print("Reading metadata db conn-params from {}".format(metadataFilePath))
        with open(metadataFilePath, 'r') as f:
            config = json.load(f)

        try:
            connection = mysql.connector.connect(host=config["host"], database=config["database"], user=config["username"],
                                                    password=config["password"], port=config["port"])
            if connection.is_connected():
                cursor = connection.cursor()
                query = "select b.account, b.warehouse, b.db, b.dbschema, b.user, b.password, b.region, b.endpoint from `Customer` a join `CustomerSnowflakeConfigProperties` b on a.customerId = b.customerId where a.`customerName`='{}' and b.`envName`='{}'".format(
                    customer, environment)
                cursor.execute(query)
                record = cursor.fetchall()
                if (len(record) > 0):
                    # Set connection params (if not overridden in the command-line)
                    if "account" not in connection_parameters:
                        connection_parameters["account"] = record[0][0]
                    if "warehouse" not in connection_parameters:
                        connection_parameters["warehouse"] = record[0][1]
                    if "database" not in connection_parameters:
                        connection_parameters["database"] = record[0][2]
                    if "schema" not in connection_parameters:
                        connection_parameters["schema"] = record[0][3]
                    if "user" not in connection_parameters:
                        connection_parameters["user"] = record[0][4]
                    if "password" not in connection_parameters:
                        connection_parameters["password"] = record[0][5]
                    if "snowflake_region" not in connection_parameters:
                        connection_parameters["snowflake_region"] = record[0][6]

                    if connection_parameters["snowflake_region"] != "":
                        connection_parameters["account"] = connection_parameters["account"] + "." + connection_parameters["snowflake_region"]

                    print('Success reading from CustomerSnowflakeConfigProperties table, account={}, pwd=****, database={}, schema={}, warehouse={}, user={}'.format(
                        connection_parameters["account"], connection_parameters["database"], connection_parameters["schema"], connection_parameters["warehouse"], connection_parameters["user"]
                    ))
                else:
                    print('Customer Information not found in CustomerSnowflakeConfigProperties table')
                    exit(1)
        except Error as e:
            print("Error", e)
            exit(1)
        finally:
            # closing database connection.
            if (connection.is_connected()):
                cursor.close()
                connection.close()


    def get_commandline_parameters(self, args):

        """
        PURPOSE:
            Read the command-line arguments and store them in a dictionary.
            Command-line arguments should come in pairs, e.g.:
                "--user MyUser"
        INPUTS:
            The command line arguments (sys.argv).
        RETURNS:
            Returns the dictionary.
        DESIRABLE ENHANCEMENTS:
            Improve error detection and handling.
        """

        connection_parameters = {}

        i = 1
        while i < len(args) - 1:
            property_name = args[i]
            # Strip off the leading "--" from the tag, e.g. from "--user".
            property_name = property_name[2:]
            property_value = args[i + 1]
            connection_parameters[property_name] = property_value
            i += 2

        try:
            usemetadata = connection_parameters["usemetadata"]
        except:
            usemetadata = ""
        
        if usemetadata == '1':
            self.retrieve_metadata(connection_parameters)

        return connection_parameters


    def create_connection(self, connection_parameters):

        """
        PURPOSE:
            This connects gets account and login information from the
            environment variables and command-line parameters, connects to the
            server, and returns the connection object.
        INPUTS:
            argv: This is usually sys.argv, which contains the command-line
                  parameters. It could be an equivalent substitute if you get
                  the parameter information from another source.
        RETURNS:
            A connection.
        """

        # Get account and login information from environment variables and
        # command-line parameters.
        # Note that ACCOUNT might require the region and cloud platform where
        # your account is located, in the form of
        #     '<your_account_name>.<region>.<cloud>'
        # for example
        #     'xy12345.us-east-1.azure')
        # -- (> ----------------------- SECTION=set_login_info ---------------

        # Get the password from an appropriate environment variable, if
        # available.

        # If the password is set by both command line and env var, the
        # command-line value takes precedence over (is written over) the
        # env var value.
        try:
            PASSWORD = connection_parameters["password"]
        except:
            PASSWORD = ""

        if PASSWORD is None or PASSWORD == '':
            connection_parameters["password"] = os.getenv('SNOWSQL_PWD')

        # Optional: for internal testing only.
        try:
            PORT = connection_parameters["port"]
            PROTOCOL = connection_parameters["protocol"]
        except:
            PORT = ""
            PROTOCOL = ""

        # If the password wasn't set either in the environment var or on
        # the command line...
        try:
            PASSWORD = connection_parameters["password"]
        except:
            PASSWORD = ""            
        if PASSWORD is None or PASSWORD == '':
            print("ERROR: Set password, e.g. with SNOWSQL_PWD environment variable")
            sys.exit(-2)
        # -- <) ---------------------------- END_SECTION ---------------------

        print("Connecting...")
        if PROTOCOL is None or PROTOCOL == "" or PORT is None or PORT == "":
            # -- (> ------------------- SECTION=connect_to_snowflake ---------
            conn = snowflake.connector.connect(
                user=connection_parameters["user"],
                password=connection_parameters["password"],
                account=connection_parameters["account"],
                warehouse=connection_parameters["warehouse"],
                database=connection_parameters["database"],
                schema=connection_parameters["schema"]
                )
            # -- <) ---------------------------- END_SECTION -----------------
        else:
            conn = snowflake.connector.connect(
                user=connection_parameters["user"],
                password=connection_parameters["password"],
                account=connection_parameters["account"],
                warehouse=connection_parameters["warehouse"],
                database=connection_parameters["database"],
                schema=connection_parameters["schema"],
                # Optional: for internal testing only.
                protocol=PROTOCOL,
                port=PORT
                )

        return conn


    def set_up(self, connection, params):

        """
        PURPOSE:
            Set up to run a test. You can override this method with one
            appropriate to your test/demo.
        """

        # Create a temporary warehouse, database, and schema.
        #self.create_warehouse_database_and_schema(connection)


    def process_db(self, connection, params):

        """
        PURPOSE:
            Your sub-class should override this to include the code required for
            your documentation sample or your test case.
            This default method does a very simple self-test that shows that the
            connection was successful.
        """

        # Create a cursor for this connection.
        cursor1 = conn.cursor()
        # This is an example of an SQL statement we might want to run.
        command = "SELECT PI()"
        # Run the statement.
        cursor1.execute(command)
        # Get the results (should be only one):
        for row in cursor1:
            print(row[0])
        # Close this cursor.
        cursor1.close()


    def clean_up(self, connection, params):

        """
        PURPOSE:
            Clean up after a test. You can override this method with one
            appropriate to your test/demo.
        """

        # Create a temporary warehouse, database, and schema.
        #self.drop_warehouse_database_and_schema(connection)


# ----------------------------------------------------------------------------

if __name__ == '__main__':
    pvb = snowflake_python_base()
    pvb.main(sys.argv)
