# Databricks notebook source
from pyspark.sql import SparkSession
from pyspark.sql import SQLContext

from pyspark.sql.types import *
from decimal import Decimal
from pyspark.sql import Window
from pyspark.sql.functions import expr, col, column
from pyspark.sql.functions import pandas_udf, PandasUDFType
from pyspark.sql.types import StructType

import pyspark.sql.functions as F
from pyspark.sql.functions import current_timestamp
from pyspark.sql.functions import desc, row_number, monotonically_increasing_id
from pyspark.sql.types import *
from pyspark.sql import Row

import pandas as pd
import snowflake.connector
import snowflake as sf

import sys

from statsmodels.distributions.empirical_distribution import ECDF
from statsmodels.tsa.statespace.exponential_smoothing import ExponentialSmoothing


"""
  #user, password, datbase passed by metadata
  user = "ml_admin"
  password = "trsky&j0912ml"
  account = "aktanapartner"
  warehouse = "AKTANADEV_WH_US_WEST_2"
  database = "RPT_DWPREPROD_QA"
  schema_scd = "SCD"

"""
user = sys.argv[1]
password = sys.argv[2]
account = sys.argv[3]
warehouse = sys.argv[4]
database = sys.argv[5]
schema_scd = sys.argv[6]

# Initialize an options dictionary to read from snowflake
def get_db_options():
  options = {
    "sfUrl": "{}.snowflakecomputing.com/".format(account), 
    "sfUser": user,
    "sfPassword": password,
    "sfDatabase": database,
    "sfSchema": schema_scd,
    "sfWarehouse": warehouse,
  }
  return options



# Read the SCD use-cases to process from Snowflake
def ingest_scd_tables():

  scd_use_cases_sdf = spark.read.format("snowflake").options(**options).option("query", 
              """SELECT 
                   SCD_OUT_DATAPOINT_KEY,
                   USE_CASE_NAME AS DATAPOINT_NAME,
                   dp.SCD_IN_USE_CASE_KEY,
                   PERIOD_NUMBER,
                   METRIC_NAME,
                   dp.PRODUCT_NAME,
                   MARKET_BASKET_NAME,
                   TARGET_VIEW_NAME,
                   REPORTING_LEVEL_NAME,
                   p.SCD_IN_PRODUCT_CONFIG_KEY 
                FROM
                   SCD.SCD_OUT_DATAPOINT dp,
                   SCD.SCD_IN_TARGET_VIEW_MAPPING tvm,
                   SCD.SCD_IN_PRODUCT_CONFIG p
                WHERE
                  dp.SCD_IN_USE_CASE_KEY = tvm.SCD_IN_USE_CASE_KEY
                  AND dp.SCD_IN_REPORTING_LEVEL_KEY = tvm.SCD_IN_REPORTING_LEVEL_KEY
                  AND dp.DIM_FREQUENCY_KEY = tvm.DIM_FREQUENCY_KEY
                  AND p.SCD_IN_PRODUCT_CONFIG_KEY = dp.SCD_IN_PRODUCT_CONFIG_KEY
                  AND COALESCE(p.IS_MARKET_CALC_CUSTOM, 0) = COALESCE(tvm.IS_MARKET_CALC_CUSTOM, 0)""").load()
  
  return scd_use_cases_sdf

# Read use-case specific view from Snowflake Sales schema
def ingest_scd_view (target_view_name, period_number, metric_name, product_name, marketbasket_name=None):
  
  if "MARKETBASKET" in target_view_name or "MARKET_SHARE" in target_view_name: 
      query = f"SELECT * from {target_view_name} where PERIOD_NUMBER = {period_number} and metric_name = '{metric_name}' and product_name = '{product_name}' and marketbasket_name = '{marketbasket_name}'" 
  else:
    if "GAP_DETECTION" in target_view_name:
       query = f"SELECT * from {target_view_name} where metric_name = '{metric_name}' and product_name = '{product_name}'" 
    else:
       query = f"SELECT * from {target_view_name} where PERIOD_NUMBER = {period_number} and metric_name = '{metric_name}' and product_name = '{product_name}'"
    
  sales_sdf = spark.read.format("snowflake").options(**options).option("query", query).load()
  
  return sales_sdf



### Function to prepare data for modeling ###
# Filter data that is too little
# 1. At least 6 months of data (i.e. MAX_DATE - MIN_DATE is more than 6 months)
# 2. At least 1 sale within 1 year of the most recent sale date (i.e. most-recent-sale-date - most-recent-date for the reporting level)
# 3. At least 40 non-zero transactions to use for modeling
def remove_inadequate_data(df, target, reporting_level_name):
   
    # Find if there are adequate records only using records with non-zero targets
    if (use_case_key == 2):
      #for gap
      nonzero_filter = col("SALES_METRIC_VALUE") > 0
      target_count = 10
    else:
      nonzero_filter = col(target) > 0
      target_count = 40
      
    filtered = df.where(nonzero_filter)

    # Find the min and max sale date, and the # of records for each reporting-level
    min_max = filtered.groupby(reporting_level_name).agg(
      F.min("SALE_DATE").alias("MIN_DATE"),
      F.max("SALE_DATE").alias("MAX_DATE"),
      F.count("SALE_DATE").alias("TARGET_COUNT")
      )
  
    # Find the most recent sale date
    # Spark throws warning with this line since it is trying to bring all data to single partition since there is no window partition --> .withColumn("max", max("value") over ())
    most_recent_date = min_max.groupby().agg(F.max("MAX_DATE").alias("MOST_RECENT_SALE_DATE")).collect()[0][0]
    min_max = min_max.withColumn("MOST_RECENT_SALE_DATE", F.lit(most_recent_date))

    # Reporting-levels must meet 3 rules:
    # 1. At least 6 months of data (i.e. MAX - MIN is more than 6 months)
    # 2. At least 1 sale within 1 year of the most recent sale date (i.e. most-recent-sale-date - most-recent-date for the reporting level)
    # 3. At least 40 non-zero transactions to use for modeling
    min_max = min_max.withColumn("DIFF_DAYS", F.datediff("MAX_DATE", "MIN_DATE"))\
      .withColumn("DAYS_TILL_MOST_RECENTSALE", F.datediff("MOST_RECENT_SALE_DATE", "MAX_DATE"))\
      .where("DIFF_DAYS > 180")\
      .where("DAYS_TILL_MOST_RECENTSALE < 365")\
      .where("TARGET_COUNT > {}".format(target_count))

    # Finally, remove records that are before min-date for a reporting level (zero's before the 1st non-zero value)
    filtered = filtered.join(min_max,[reporting_level_name]).where(col("SALE_DATE") >= col("MIN_DATE"))
    return(filtered)
  



ret_columns = ['reporting_level_name','sale_date','observed', 'predicted','scd_name','Score', 
               'product_name', 'scd_out_datapoint_key','change_direction']
schema = StructType([
    StructField("BRICK_HCO_CODE", StringType(), False), #reporting value
    StructField("reporting_level_name", StringType(), True), #reporting name
    StructField("sale_date", DateType(), True),  
    StructField("observed", FloatType(), True),
    StructField("predicted", FloatType(), True),
    StructField("scd_name", StringType(), True),
    StructField("Score", FloatType(), True),
    StructField("product_name", StringType(), True),
    StructField("scd_out_datapoint_key", DecimalType(), True),
    StructField("change_direction", StringType(), True)
  
])




def anomaly_fn(df, target, reporting_level_name, alpha_up, alpha_down):
  @pandas_udf(schema, PandasUDFType.GROUPED_MAP)
  def anomaly_detect(df):
  
    group_key = df[reporting_level_name].iloc[0]
 
    x = df[target].astype(float).tolist()
    ret_sale_date= df['SALE_DATE'].iloc[-1]

    min_date = df['SALE_DATE'].min()
    series_date = pd.date_range(min_date, periods=len(x), freq='W-SAT')
    data = pd.Series(x, series_date)

    up = "SCD_",product_name.title(),"_",target_view_name.split("_", 3)[-1].title(),"_",metric_name,"_Increasing_",str(period_number),"w"
    down="SCD_",product_name.title(),"_",target_view_name.split("_", 3)[-1].title(),"_",metric_name,"_Decreasing_",str(period_number),"w"
     
    model = ExponentialSmoothing(data, trend=True, seasonal=4)
    results = model.fit(low_memory=True, method='powell', disp=0)

    predicted = results.fittedvalues[-1]
    resid = results.resid[-1]
    lower = results.resid.quantile(q=.25)
    upper = results.resid.quantile(q=.75)
    iqr = upper - lower
    
    if (predicted < 0):
      predicted = 0
    if (resid > upper):
      SCD_up = 1 - ((alpha_up * iqr) / (resid - upper))
    else:
      SCD_up = 0.0

    if (resid < lower): 
      SCD_down = 1 + ((alpha_down * iqr ) / (resid - lower))
    else:
      SCD_down = 0.0
  
    if (SCD_up >=100):
        SCD_up = 99.99
    if (SCD_up <0):
       SCD_up = 0
    if (SCD_down < 0):
        SCD_down =0
    if (SCD_down >=100):
       SCD_down = 99.99 
  
    ret_product_name = product_name 
    ret_scd_out_datapoint_key = scd_out_datapoint_key 
    ret_reporting_level_name = reporting_level_name_string.split("_", 1)[0].upper()
    ret_change_direction_increasing = 'Increasing' 
    ret_change_direction_decreasing = 'Decreasing' 
    
    return pd.DataFrame([[group_key] + [ret_reporting_level_name, ret_sale_date, x[-1], predicted, "".join(up), SCD_up, 
                                        product_name, scd_out_datapoint_key,  ret_change_direction_increasing]], 
                                        columns= [reporting_level_name] + ret_columns).append(
           pd.DataFrame([[group_key] + [ret_reporting_level_name, ret_sale_date, x[-1], predicted, "".join(down), SCD_down,
                                         product_name, scd_out_datapoint_key, ret_change_direction_decreasing]], 
                                         columns=[reporting_level_name] + ret_columns))
  
  return df.groupby(reporting_level_name).apply(anomaly_detect)



gap_ret_columns = ['reporting_level_name','sale_date','observed', 'predicted','scd_name','Score', 
                   'product_name', 'scd_out_datapoint_key', 'change_direction']

schema = StructType([
    StructField("BRICK_HCO_CODE", StringType(), False), #reporting value
    StructField("reporting_level_name", StringType(), True), #reporting name
    StructField("sale_date", DateType(), True),  
    StructField("observed", FloatType(), True),
    StructField("predicted", FloatType(), True),
    StructField("scd_name", StringType(), True),
    StructField("Score", FloatType(), True),
    StructField("product_name", StringType(), True),
    StructField("scd_out_datapoint_key", DecimalType(), True),
    StructField("change_direction", StringType(), True)])



def gap_detect_fn(df, target, reporting_level_name):
  @pandas_udf(schema, PandasUDFType.GROUPED_MAP)
  def gap_detect(df):
    group_key = df[reporting_level_name].iloc[0]
    # for Order Gap Down lastDiff is the number of days since the last order
    lastDiff = float(df[target].iloc[-1])
 
    # estimate the ecdf function fn() using order_gap_week by creating vector of purchase times
    x = df[df.SALES_METRIC_VALUE!=0][target].astype(float)
    fn = ECDF(x)
  
    observed = float(lastDiff)
    predicted = x.quantile().astype(float)
    sub_string = "SCD_",product_name.title(),"_",target_view_name.split("_", 3)[-1].title(),"_",metric_name,"_Increasing_",str(period_number),"w"
    scd_name = "".join(sub_string)
    Score = fn(lastDiff).round(4)*100.0

    ret_sale_date= df['SALE_DATE'].iloc[-1]
  
    ret_product_name = product_name 
    ret_scd_out_datapoint_key = scd_out_datapoint_key 
    ret_reporting_level_name = reporting_level_name_string.split("_", 1)[0].upper()
    
    ret_change_direction= 'Increasing' 
  
    # adjust for rounding issues
    if (observed == predicted):
      Score = 0.0
        
    # adjust for too much confidence
    if (Score == 100.0):
      Score = 99.0
  
    prob_up = pd.DataFrame([[group_key] + [ret_reporting_level_name, ret_sale_date, lastDiff, predicted, scd_name, Score,
                                           ret_product_name, ret_scd_out_datapoint_key, ret_change_direction]], 
                                           columns=[reporting_level_name] + gap_ret_columns)
    # for prob_down
    last_order_gap = df[target][df.SALES_METRIC_VALUE != 0].iloc[-1]
    observed = float(last_order_gap)
    sub_string = "SCD_", product_name.title(), "_", target_view_name.split("_", 3)[
          -1].title(), "_", metric_name, "_Decreasing_", str(period_number), "w"
    scd_name = "".join(sub_string)
    Score = (1 - fn(last_order_gap)).round(4) * 100
    
    # adjust for rounding issues
    if (observed == predicted):
      Score = 0.0
        
    # adjust for too much confidence
    if (Score == 100.0):
      Score = 99.0
      
    ret_change_direction = 'Decreasing'
  
    prob_down = pd.DataFrame(
          [[group_key] + [ret_reporting_level_name, ret_sale_date, observed, predicted, scd_name, Score,
                          ret_product_name, ret_scd_out_datapoint_key, ret_change_direction]], 
                          columns=[reporting_level_name] + gap_ret_columns)
    
    prob = pd.concat([prob_up, prob_down])
  
    return prob
  return df.groupby(reporting_level_name).apply(gap_detect)
  

def create_metrics(create_df, product_name, reporting_level_name, scd_out_datapoint_key):
    # data for anomalies up
  
    create_df_pd = create_df.toPandas()
    scd_names = create_df_pd["scd_name"].unique()
    
    if len(scd_names) == 1:
        use_case_name = scd_names[0] if "Increasing" in scd_names[0] else None
    else:
        use_case_name = scd_names[0] if "Increasing" in scd_names[0] else scd_names[1]
    
    # table for anomalies up
    rep_count = len(create_df_pd["reporting_level_name"].unique())
    sale_date_pandas = create_df_pd["sale_date"].unique()
    sale_date_list = list(sale_date_pandas)
    row_num = len(sale_date_list)
    
    change_direction = 'Increasing'
    
    if use_case_name is not None:
        x = create_df_pd[create_df_pd.scd_name == use_case_name]["Score"]
        #x = create_df[create_df.scd_name == use_case_name].select("Score")
        #x_list = x.collect()
        x_total = 0
        x_score = [0 for i in range(5)]
        for row in x:
            if row >= 75:
                x_score[0] += 1
            elif row >= 80:
                x_score[1] += 2
            elif row >= 85:
                x_score[2] += 1
            elif row >= 90:
                x_score[3] += 1
            elif row >= 95:
                x_score[4] += 1
            x_total += 1
        df_up = pd.DataFrame({
            'CURRENT_WEEK_DATE': sale_date_list * 5,
            'SCD_NAME': [use_case_name for i in range(5 * row_num)],
            'ELIGIBILITY_PERCENT': [(s / x_total) * 100 for s in x_score] * row_num,
            'ANOMALY_THRESHOLD': [.75, .80, .85, .90, .95] * row_num,
            'REPORTING_LEVEL_COUNT': [rep_count for i in range(5 * row_num)],
            'PRODUCT_NAME': [product_name for i in range(5 * row_num)],
            'REPORTING_LEVEL_NAME': [reporting_level_name for i in range(5 * row_num)],
            'SCD_OUT_DATAPOINT_KEY': [scd_out_datapoint_key for i in range(5 * row_num)],
            'CHANGE_DIRECTION': [change_direction for i in range(5 * row_num)]})
    else:
        df_up = None
        
    # data for anomalies down
    if len(scd_names) == 1:
       use_case_name = None if "Increasing" in scd_names[0][0] else scd_names[0][0]
    else:
       use_case_name = scd_names[1][0] if "Increasing" in scd_names[0][0] else scd_names[0][0]
        # table for anomalies down
    change_direction = 'Decreasing'
    if use_case_name is not None:
      #y = create_df[create_df.scd_name == use_case_name].select("Score")
      #y_list = y.collect()
      y = create_df_pd[create_df_pd.scd_name == use_case_name]["Score"]
      y_total = 0
      y_score = [0 for i in range(5)]
      for row in y:
        if row >= 75:
            y_score[0] += 1
        elif row >= 80:
            y_score[1] += 2
        elif row >= 85:
            y_score[2] += 1
        elif row >= 90:
            y_score[3] += 1
        elif row >= 95:
            y_score[4] += 1
        y_total += 1
        df_down = pd.DataFrame({
                'CURRENT_WEEK_DATE': sale_date_list * 5,
                'SCD_NAME': [use_case_name for i in range(5 * row_num)],
                'ELIGIBILITY_PERCENT': [(s / y_total) * 100 for s in y_score] * row_num,
                'ANOMALY_THRESHOLD': [.75, .80, .85, .90, .95] * row_num,
                'REPORTING_LEVEL_COUNT': [rep_count for i in range(5 * row_num)],
                'PRODUCT_NAME': [product_name for i in range(5 * row_num)],
                'REPORTING_LEVEL_NAME': [reporting_level_name for i in range(5 * row_num)],
                'SCD_OUT_DATAPOINT_KEY': [scd_out_datapoint_key for i in range(5 * row_num)],
                'CHANGE_DIRECTION': [change_direction for i in range(5 * row_num)]})
      else:
         df_down = None
    
    # combine up and down for full use case metrics
    if df_up is not None and df_down is not None:
        create_df = pd.concat([df_up, df_down])
    elif df_up is not None:
        create_df = df_up
    elif df_down is not None:
        create_df = df_down
    else:
        create_df = None
    df_spark = spark.createDataFrame(create_df)
    return (df_spark)



# Initialize spark and sql context
spark = SparkSession.builder.appName("SCD2 Model").getOrCreate()
sqlContext = SQLContext(sparkContext=spark.sparkContext, sparkSession=spark)

sc = spark.sparkContext
options = get_db_options()

scd_out_param = ingest_scd_tables()
rows = scd_out_param.rdd.collect()



schema_output = StructType([
    StructField("REPORTING_LEVEL_VALUE", StringType(), True),
    StructField("REPORTING_LEVEL_NAME", StringType(), True),
    StructField("CURRENT_WEEK_DATE", DateType(), True),
    StructField("SCD_ACTUAL_VALUE", FloatType(), True),
    StructField("SCD_PREDICTED_VALUE", FloatType(), True), 
    StructField("SCD_NAME", StringType(), True),
    StructField("SCD_SCORE", FloatType(), True), 
    StructField("PRODUCT_NAME", StringType(), True),
    StructField("SCD_OUT_DATAPOINT_KEY", IntegerType(), True),
    StructField("CHANGE_DIRECTION", StringType(), True),
])




schema_bi_metric = StructType([
    StructField("CURRENT_WEEK_DATE", DateType(), True),
    StructField("SCD_NAME", StringType(), True),
    StructField("ELIGIBILITY_PERCENT", FloatType(), True),
    StructField("ANOMALY_THRESHOLD", FloatType(), True),  
    StructField("REPORTING_LEVEL_COUNT", IntegerType(), True),
    StructField("PRODUCT_NAME", StringType(), True),
    StructField("REPORTING_LEVEL_NAME", StringType(), True),
    StructField("SCD_OUT_DATAPOINT_KEY", IntegerType(), True),
    StructField("CHANGE_DIRECTION", StringType(), True),
])

df_output = sqlContext.createDataFrame(sc.emptyRDD(), schema_output)


df_metrics = sqlContext.createDataFrame(sc.emptyRDD(), schema_bi_metric)



def processRow(use_case_key, target_view_name, scd_out_datapoint_key, period_number, metric_name, product_name,market_basket_name):
  
    if use_case_key ==3 or use_case_key ==4:
      vol = ingest_scd_view (target_view_name, period_number, metric_name, product_name,market_basket_name)
    else:
      vol = ingest_scd_view (target_view_name, period_number, metric_name, product_name)
    
    if target_view_name.find('BRICK') != -1:  # use brick_hco_code for BRICK
      reporting_level_name = vol.columns[0]
    else:
      reporting_level_name = vol.columns[3]  # use sales_reference_account_id for ACCOUNT
  
    target = vol.columns[-1]
   
    if (use_case_key == 1):
      alpha_up = 0.25
      alpha_down = 0.15
    if (use_case_key == 3):
      alpha_up = 0.25
      alpha_down = 0.05
    if (use_case_key == 4):
      alpha_up = 0.05
      alpha_down = 0.25
      
    vol_clean = remove_inadequate_data(vol, target, reporting_level_name)
    
    if (use_case_key == 2):
      vol_model = gap_detect_fn(vol_clean, target, reporting_level_name)
    else:
      vol_model = anomaly_fn(vol_clean, target, reporting_level_name, alpha_up, alpha_down)
    
    
    vol_metric = create_metrics(vol_model,product_name,reporting_level_name, scd_out_datapoint_key)
    

    return vol_model, vol_metric
 



for row in rows:
  use_case_key = row.SCD_IN_USE_CASE_KEY
  period_number = row.PERIOD_NUMBER
  metric_name = row.METRIC_NAME
  product_name = row.PRODUCT_NAME
  target_view_name = row.TARGET_VIEW_NAME
  scd_out_datapoint_key = row.SCD_OUT_DATAPOINT_KEY
  reporting_level_name_string = row.REPORTING_LEVEL_NAME
  market_basket_name  = row.MARKET_BASKET_NAME
  
  vol_model, vol_metric= processRow(use_case_key, target_view_name, scd_out_datapoint_key, period_number, metric_name, product_name, market_basket_name)
  
  df_output = df_output.union(vol_model)
  df_metrics= df_metrics.union(vol_metric)
  
  

df_output = df_output.withColumn("SCD_POST_PROCESS_OUTPUT_KEY", monotonically_increasing_id())

df_metrics = df_metrics.withColumn("SCD_POST_PROCESS_BI_METRIC_OUTPUT_KEY", monotonically_increasing_id())


df_output = df_output.withColumn('CREATED_TS', F.current_timestamp())
df_output = df_output.withColumn('UPDATED_TS', F.current_timestamp())

#To put in the right order, scd_post_process_output_key has to be 1st column.  

df_output = df_output.select("SCD_POST_PROCESS_OUTPUT_KEY","SCD_NAME","REPORTING_LEVEL_NAME","REPORTING_LEVEL_VALUE","PRODUCT_NAME","CURRENT_WEEK_DATE","SCD_ACTUAL_VALUE","SCD_PREDICTED_VALUE","SCD_SCORE","CREATED_TS","UPDATED_TS","SCD_OUT_DATAPOINT_KEY","CHANGE_DIRECTION")



df_metrics = df_metrics.withColumn('CREATED_TS', F.current_timestamp())
df_metrics = df_metrics.withColumn('UPDATED_TS', F.current_timestamp())

df_metrics = df_metrics.select("SCD_POST_PROCESS_BI_METRIC_OUTPUT_KEY","SCD_NAME","REPORTING_LEVEL_NAME","PRODUCT_NAME","CURRENT_WEEK_DATE","ELIGIBILITY_PERCENT","ANOMALY_THRESHOLD", "REPORTING_LEVEL_COUNT","CREATED_TS","UPDATED_TS","SCD_OUT_DATAPOINT_KEY","CHANGE_DIRECTION")



con = sf.connector.connect(
  user=user,
  password=password,
  account=account,
  database=database, 
  schema=schema_scd
)
cur = con.cursor()
try:
    cur.execute("truncate table SCD_POST_PROCESS_OUTPUT")
    cur.execute("truncate table SCD_POST_PROCESS_BI_METRIC_OUTPUT")
finally:
    cur.close()
con.close()



df_output.write.format("snowflake").options(**options).mode("append").option("dbtable", "SCD_POST_PROCESS_OUTPUT").option("continue_on_error","on").save()


df_output.write.format("snowflake").options(**options).mode("append").option("dbtable", "SCD_POST_PROCESS_OUTPUT_ARCHIVE").option("continue_on_error","on").save()


df_metrics.write.format("snowflake").options(**options).mode("append").option("dbtable",
"SCD_POST_PROCESS_BI_METRIC_OUTPUT").option("continue_on_error","on").save()
df_metrics.write.format("snowflake").options(**options).mode("append").option("dbtable",
"SCD_POST_PROCESS_BI_METRIC_OUTPUT_ARCHIVE").option("continue_on_error","on").save()