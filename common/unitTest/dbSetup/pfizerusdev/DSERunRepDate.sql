CREATE TABLE `DSERunRepDate` (
  `runRepDateId` bigint(20) NOT NULL,
  `runId` bigint(20) NOT NULL,
  `repId` int(11) NOT NULL,
  `repUID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `suggestedDate` date NOT NULL,
  PRIMARY KEY (`runRepDateId`),
  UNIQUE KEY `dserunrepdate_AK1` (`runId`,`repId`,`suggestedDate`),
  KEY `dserunrepdate_parent` (`runId`),
  CONSTRAINT `dserunrepdate_ibfk_1` FOREIGN KEY (`runId`) REFERENCES `DSERun` (`runId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
