CREATE TABLE `AccountTimeToEngage` (
  `accountUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `channelUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `predictionDate` date DEFAULT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probability` double DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;